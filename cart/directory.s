;
; Karri Kaksonen, 2015
;

	.import __BLOCKSIZE__
	.import __STARTOFDIRECTORY__

	.export _MAIN_FILENR : absolute
	.import __STARTUP_LOAD__
	.import __STARTUP_SIZE__
	.import __INIT_SIZE__
	.import __CODE_SIZE__
	.import __DATA_SIZE__
	.import __RODATA_SIZE__

	.export _HM_FILENR : absolute
	.import __HM_START__
	.import __HM_CODE_SIZE__
	.import __HM_DATA_SIZE__
	.import __HM_RODATA_SIZE__
	.export _SFX_FILENR : absolute
	.import __SFX_START__
	.import __SFX_RODATA_SIZE__
	.export _SAMPLES_FILENR : absolute
	.import __SAMPLES_START__
	.import __SAMPLES_RODATA_SIZE__
	.export _SAMPLES1_FILENR : absolute
	.import __SAMPLES1_START__
	.import __SAMPLES1_RODATA_SIZE__
	.export _MUSIC_FILENR : absolute
	.import __MUSIC_START__
	.import __MUSIC_RODATA_SIZE__

	.export _INTRO_FILENR : absolute
	.import __INTRO_START__
	.import __INTRO_CODE_SIZE__
	.import __INTRO_DATA_SIZE__
	.import __INTRO_RODATA_SIZE__

	.export _GAME_FILENR : absolute
	.import __GAME_CODE_LOAD__
	.import __GAME_CODE_SIZE__
	.import __GAME_DATA_SIZE__
	.import __GAME_RODATA_SIZE__

	.segment "DIRECTORY"

__DIRECTORY_START__:

.macro entry old_off, old_len, new_off, new_block, new_len, new_size, new_addr
new_off=old_off+old_len
new_block=new_off/__BLOCKSIZE__
new_len=new_size
	.byte	<new_block
	.word	(new_off & (__BLOCKSIZE__ - 1))
	.byte	$88
	.word	new_addr
	.word	new_len
.endmacro

; Entry 0 - first executable
_MAIN_FILENR=0
entry __STARTOFDIRECTORY__+(__DIRECTORY_END__-__DIRECTORY_START__), 0, mainoff, mainblock, mainlen, __STARTUP_SIZE__+__INIT_SIZE__+__CODE_SIZE__+__RODATA_SIZE__+__DATA_SIZE__, __STARTUP_LOAD__

_HM_FILENR=_MAIN_FILENR+1
entry mainoff, mainlen, hmoff, hmblock, hmlen, __HM_CODE_SIZE__+__HM_RODATA_SIZE__+__HM_DATA_SIZE__, __HM_START__

_SFX_FILENR=_HM_FILENR+1
entry hmoff, hmlen, sfxoff, sfxblock, sfxlen, __SFX_RODATA_SIZE__, __SFX_START__

_SAMPLES_FILENR=_SFX_FILENR+1
entry sfxoff, sfxlen, samplesoff, samplesblock, sampleslen, __SAMPLES_RODATA_SIZE__, 0

_SAMPLES1_FILENR=_SAMPLES_FILENR+1
entry samplesoff, sampleslen, samples1off, samples1block, samples1len, __SAMPLES1_RODATA_SIZE__, 0

_MUSIC_FILENR=_SAMPLES1_FILENR+1
entry samples1off, samples1len, musicoff, musicblock, musiclen, __MUSIC_RODATA_SIZE__, __MUSIC_START__

_INTRO_FILENR=_MUSIC_FILENR+1
entry musicoff, musiclen, introoff, introblock, introlen, __INTRO_CODE_SIZE__+__INTRO_RODATA_SIZE__+__INTRO_DATA_SIZE__, __INTRO_START__

_GAME_FILENR=_INTRO_FILENR+1
entry introoff, introlen, gameoff, gameblock, gamelen,__GAME_CODE_SIZE__+__GAME_RODATA_SIZE__+__GAME_DATA_SIZE__, __GAME_CODE_LOAD__

__DIRECTORY_END__:

