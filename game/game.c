#include <lynx.h>
#include <conio.h>
#include <joystick.h>
#include <tgi.h>
#include <stdlib.h>
#include <time.h>

#define PickSFX 0
#define PlingSFX 1
#define BombSFX 2
#define MachinegunSFX 3
#define ChopperSFX 4
#define ChopperSFXpriority 236
#define AlertSFX 5
#define ExplosionSFX 6
#define StepSFX 7
#define LocknloadSFX 8
extern void __fastcall__ HandyMusic_PlaySFX(unsigned char sfx);
extern void HandyMusic_StopMusic ();
extern void __fastcall__ PlayPCMSample(unsigned char sample);
unsigned char checkInput (void);
extern unsigned char reset;
extern unsigned char mode;
extern unsigned char bgmusic;
unsigned char currentmode;

extern void __fastcall__ openn(int filenr);
extern int SAMPLES_FILENR;

typedef struct
{
  unsigned char *music0;
  unsigned char *music1;
  unsigned char *music2;
  unsigned char *music3;
} song_t;

extern song_t musicptr;

int curx = 0;
int cury = 0;

static unsigned char lcdpalette[] = {
  0xfff >> 8,
  0x000 >> 8,
  0x002 >> 8,
  0x113 >> 8,
  0x115 >> 8,
  0x226 >> 8,
  0x228 >> 8,
  0x229 >> 8,
  0x33b >> 8,
  0x33c >> 8,
  0x55c >> 8,
  0x77d >> 8,
  0x88d >> 8,
  0xaae >> 8,
  0xbbe >> 8,
  0xddf >> 8,

  0xfff & 0xff,
  0x000 & 0xff,
  0x002 & 0xff,
  0x113 & 0xff,
  0x115 & 0xff,
  0x226 & 0xff,
  0x228 & 0xff,
  0x229 & 0xff,
  0x33b & 0xff,
  0x33c & 0xff,
  0x55c & 0xff,
  0x77d & 0xff,
  0x88d & 0xff,
  0xaae & 0xff,
  0xbbe & 0xff,
  0xddf & 0xff
};

extern unsigned char LCDgame[];
extern unsigned char empty[];
extern unsigned char one[];
extern unsigned char two[];
extern unsigned char three[];
extern unsigned char four[];
extern unsigned char five[];
extern unsigned char six[];
extern unsigned char bomb[];
extern unsigned char heart[];
extern unsigned char curs[];
extern unsigned char flag[];
extern unsigned char easy[];
extern unsigned char normal[];
extern unsigned char hard[];
extern unsigned char note[];
#define UNSEEN 0x80
#define THINK_BOMB 0x40
#define ACTUAL_BOMB 0x20
#define CORRECT 0x10
#define EMPTY 0
#define CURS 7
#define HEART 8
#define GAME_WAIT 13
#define GAME_READY 9
#define GAME_WON 10
#define GAME_LOST 11
#define GAME_CONTINUES 12

static SCB_REHV_PAL Sbkg = {
  BPP_4 | TYPE_BACKGROUND,
  0x10, 0x20,
  0,
  LCDgame,
  0, 0,
  0x0100, 0x100,
  {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef}
};

static SCB_REHV_PAL Spath = {
  BPP_2 | TYPE_NORMAL,
  0x10, 0x20,
  0,
  one,
  30, 7,
  0x0100, 0x100,
  {0x01, 0x4f}
};

static SCB_REHV_PAL Snote = {
  BPP_2 | TYPE_NORMAL,
  0x10, 0x20,
  0,
  note,
  13, 89,
  0x0100, 0x100,
  {0x01, 0x4f}
};

static SCB_REHV_PAL Smode = {
  BPP_1 | TYPE_NORMAL,
  0x10, 0x20,
  0,
  hard,
  1, 1,
  0x0100, 0x100,
  {0x01}
};

extern int status;

static unsigned char heartstep = 99;

static void
showheart (unsigned char x)
{
  if (status == GAME_WON)
    {
      Spath.data = empty;
      switch (heartstep)
	{
	case 0:
	  if (x != 0 && x != 8)
	    {
	      return;
	    }
	  break;
	case 1:
	  if (x != 1 && x != 7)
	    {
	      return;
	    }
	  break;
	case 2:
	  if (x != 2 && x != 6)
	    {
	      return;
	    }
	  break;
	case 3:
	  if (x != 3 && x != 5)
	    {
	      return;
	    }
	  break;
	case 4:
	  if (x != 4)
	    {
	      return;
	    }
	  break;
	case 5:
	  if (x < 3 || x > 5)
	    {
	      return;
	    }
	  break;
	case 6:
	  if (x < 2 || x > 6)
	    {
	      return;
	    }
	  break;
	case 7:
	  if (x < 1 || x > 7)
	    {
	      return;
	    }
	  break;
	default:
	  break;
	}
      Spath.data = heart;
    }
}

static void
drawblock (unsigned char block, unsigned char x, unsigned char y)
{
  switch (block & ~CORRECT)
    {
    case CURS:
      Spath.data = curs;
      break;
    case 1:
      Spath.data = one;
      showheart (x);
      break;
    case 2:
      Spath.data = two;
      showheart (x);
      break;
    case 3:
      Spath.data = three;
      showheart (x);
      break;
    case 4:
      Spath.data = four;
      showheart (x);
      break;
    case 5:
      Spath.data = five;
      showheart (x);
      break;
    case 6:
      Spath.data = six;
      showheart (x);
      break;
    case ACTUAL_BOMB:
      Spath.data = bomb;
      break;
    case HEART:
      Spath.data = heart;
      break;
    case EMPTY:
      Spath.data = empty;
      showheart (x);
      break;
    default:
      if (((block & THINK_BOMB) == THINK_BOMB) &&
	  ((block & UNSEEN) == UNSEEN))
	{
	  Spath.data = flag;
	}
      else
	{
	  return;
	}
      break;
    }
  Spath.hpos = 30 + 10 * x;
  Spath.vpos = 7 + 12 * y + (x & 1 ? -6 : 0);
  tgi_sprite (&Spath);
}

/***** code borrowed from XBomb project *****/
/***************************************
  XBomb - 'Minesweeper' game - Version 2.2b.

  Main function of program - excludes all window interface parts.
		        ******************//******************
  Written by Andrew M. Bishop

  This file Copyright 1994-2014 Andrew M. Bishop
  It may be distributed under the GNU Public License, version 2, or
  any higher version.  See section COPYING of the GNU Public license
  for conditions under which this file may be redistributed.
  ***************************************/

#define WIDTH 9
#define HEIGHT 8
static unsigned char bombs = 12;

static unsigned char state[WIDTH][HEIGHT];

/*+ The status of the game, +*/
static int n_unseen,		/*+ the number of tiles unseen. + */
  n_think,			/*+ the number of flags put down. + */
  ticks;			/*+ The number of milliseconds of time passed. + */


static int count_adjacent (int x, int y, int flag);

static void
StopGame (void)
{
  status = GAME_WAIT;
}

static void
MarkBomb (int x, int y)
{
  if (((state[x][y] & UNSEEN) == 0) || status == GAME_WAIT)
    return;

  state[x][y] ^= THINK_BOMB;
}

static void
RemoveEmpties (int x, int y)
{
  if (x < 0 || x >= WIDTH)
    return;

  if (y < 0 || y >= HEIGHT)
    return;

  if (!(state[x][y] & UNSEEN) || (state[x][y] & THINK_BOMB))
    return;

  state[x][y] &= ~UNSEEN;
  n_unseen--;

  if (state[x][y] != EMPTY)
    return;

  if (x & 1 == 1)
    {
      RemoveEmpties (x - 1, y - 1);
      RemoveEmpties (x, y - 1);
      RemoveEmpties (x + 1, y - 1);
      RemoveEmpties (x - 1, y);
      RemoveEmpties (x + 1, y);
      RemoveEmpties (x, y + 1);
    }
  else
    {
      RemoveEmpties (x, y - 1);
      RemoveEmpties (x - 1, y);
      RemoveEmpties (x + 1, y);
      RemoveEmpties (x - 1, y + 1);
      RemoveEmpties (x, y + 1);
      RemoveEmpties (x + 1, y + 1);
    }
}

static int
count_adjacent (int x, int y, int flag)
{
  int n = 0, dx, dy, dd = 1;

  for (dx = -1; dx <= 1; dx++)
    {
      if ((x + dx) < 0 || (x + dx) >= WIDTH)
	continue;

      for (dy = -1; dy <= 1; dy++)
	{
	  if ((y + dy) < 0 || (y + dy) >= HEIGHT)
	    continue;

	  if ((x & 1) == 1 && dy == 1 && (dx == -1 || dx == 1))
	    continue;

	  if ((x & 1) == 0 && dy == -1 && (dx == -1 || dx == 1))
	    continue;

	  if (state[x + dx][y + dy] & flag)
	    n++;
	}
    }

  return (n);
}
/***** end of code borrowed from XBomb project *****/

clock_t hearttimer;

void
drawscreen ()
{
  unsigned char x, y;
  tgi_sprite (&Sbkg);
  Smode.hpos = 1;
  Smode.vpos = 1;
  Smode.data = hard;
  Smode.penpal[0] = (currentmode == 2) ? 0x4f : 0x04;
  tgi_sprite (&Smode);
  Smode.vpos += 6;
  Smode.data = normal;
  Smode.penpal[0] = (currentmode == 1) ? 0x4f : 0x04;
  tgi_sprite (&Smode);
  Smode.vpos += 6;
  Smode.data = easy;
  Smode.penpal[0] = (currentmode == 0) ? 0x4f : 0x04;
  tgi_sprite (&Smode);
  Snote.penpal[0] = (bgmusic == 1) ? 0x0f : 0x04;
  Snote.penpal[1] = (bgmusic == 1) ? 0x44 : 0x00;
  tgi_sprite (&Snote);

  for (x = 0; x < WIDTH; x++)
    for (y = 0; y < HEIGHT; y++)
      drawblock (state[x][y], x, y);
  drawblock (CURS, curx, cury);
}

clock_t game_time;

static void
StartGame ()
{
  int x, y;

  n_think = 0;

  status = GAME_READY;

  for (x = 0; x < WIDTH; x++)
    for (y = 0; y < HEIGHT; y++)
      state[x][y] = UNSEEN;
}

/*++++++++++++++++++++++++++++++++++++++
  Hides the bombs in the grid.

  int xs The location that has no neighbouring bombs.

  int ys The location that has no neighbouring bombs.
  ++++++++++++++++++++++++++++++++++++++*/

void
HideBombs (int xs, int ys)
{
  int i, x, y;

  srand (clock ());
  state[xs][ys] |= CORRECT;

  for (i = 0; i < bombs; i++)
    {
      do
	{
	  x = rand () % WIDTH;
	  y = rand () % HEIGHT;
	}
      while ((state[x][y] != UNSEEN) || count_adjacent (x, y, CORRECT));

      state[x][y] |= ACTUAL_BOMB;
    }

  state[xs][ys] &= ~CORRECT;

  for (x = 0; x < WIDTH; x++)
    for (y = 0; y < HEIGHT; y++)
      if (!(state[x][y] & ACTUAL_BOMB))
	state[x][y] += count_adjacent (x, y, ACTUAL_BOMB);

  game_time = clock ();
}

void
SelectSquare (int x, int y)
{
  if (!(state[x][y] & UNSEEN) || (state[x][y] & THINK_BOMB)
      || status == GAME_WAIT)
    return;

  if (status == GAME_READY)
    {
      HideBombs (x, y);
      status = GAME_CONTINUES;
    }

  RemoveEmpties (x, y);

  if (state[x][y] & ACTUAL_BOMB)
    {
      int x, y;

      for (x = 0; x < WIDTH; x++)
	for (y = 0; y < HEIGHT; y++)
	  if (state[x][y] & UNSEEN)
	    {
	      state[x][y] &= ~UNSEEN;
	      if ((state[x][y] & THINK_BOMB) && (state[x][y] & ACTUAL_BOMB))
		state[x][y] |= CORRECT;
	    }
	  else
	    state[x][y] |= CORRECT;

      status = GAME_LOST;
      HandyMusic_PlaySFX(ExplosionSFX);
    }
}

/*++++++++++++++++++++++++++++++++++++++
  Select all of the squares adjacent

  int x The location of the square.

  int y The location of the square.
  ++++++++++++++++++++++++++++++++++++++*/

void
SelectAdjacent (int x, int y)
{
  int dx, dy, n, dd = 1;

  if ((state[x][y] & UNSEEN) || status == GAME_WAIT)
    return;

  n = count_adjacent (x, y, THINK_BOMB);

  if (state[x][y] != n)
    return;

  for (dx = -dd; dx <= dd; dx++)
    {
      if ((x + dx) < 0 || (x + dx) >= WIDTH)
	continue;

      for (dy = -1; dy <= 1; dy++)
	{
	  if ((y + dy) < 0 || (y + dy) >= HEIGHT)
	    continue;
	  if (dx == 0 && dy == 0)
	    continue;

	  if ((x & 1) == 1 && dy == 1 && (dx == -1 || dx == 1))
	    continue;

	  if ((x & 1) == 0 && dy == -1 && (dx == -1 || dx == 1))
	    continue;

	  if (!(state[x + dx][y + dy] & THINK_BOMB))
	    SelectSquare (x + dx, y + dy);
	}
    }
}

/*++++++++++++++++++++++++++++++++++++++
  Select a single square and clear it, explode the bomb or clear the adjacent.

  int x The location of the square.

  int y The location of the square.
  ++++++++++++++++++++++++++++++++++++++*/

void
SelectSquareOrAdjacent (int x, int y)
{
  if (state[x][y] & UNSEEN)
    SelectSquare (x, y);
  else				/* if(state[x][y]&EMPTY) */
    SelectAdjacent (x, y);
}

static unsigned char
fillpath ()
{
  unsigned char x;
  unsigned char y;
  for (y = 0; y < HEIGHT; y++)
    {
      for (x = 0; x < WIDTH; x++)
	{
	  if (x == 0)
	    {
	      if ((state[x][y] & UNSEEN) == 0)
		{
		  state[x][y] |= CORRECT;
		}
	    }
	  else
	    {
	      if (count_adjacent (x, y, CORRECT))
		{
		  if ((state[x][y] & UNSEEN) == 0)
		    {
		      state[x][y] |= CORRECT;
		    }
		}
	    }
	}
    }
  for (y = 0; y < HEIGHT; y++)
    {
      if ((state[WIDTH - 1][y] & CORRECT) == CORRECT)
	{
	  return 1;
	}
    }
  return 0;
}

#define DELAY_STEP 6

void
game ()
{
  unsigned char fillcnt;
  unsigned char WaitForRelease;
  clock_t now = clock () + DELAY_STEP;
  tgi_setpalette (lcdpalette);
  StartGame ();

  while (joy_read (JOY_1));
  while (!reset)
    {
      if (!tgi_busy ())
	{
	  unsigned char joy;
	  if (status == GAME_READY)
	    {
	      currentmode = mode;
	      switch (currentmode)
		{
		case 0:
		  bombs = 12;
		  break;
		case 1:
		  bombs = 15;
		  break;
		case 2:
		  bombs = 20;
		  break;
		}
	    }
	  joy = checkInput ();
	  if (joy == 0)
	    WaitForRelease = 0;
	  if (clock () > now)
	    {
	      now = clock () + DELAY_STEP;
	      if (JOY_BTN_UP (joy))
		{
		  cury--;
		  if (cury < 0)
		    cury = 0;
                  else
                    HandyMusic_PlaySFX(StepSFX);
		}
	      if (JOY_BTN_DOWN (joy))
		{
		  cury++;
		  if (cury > HEIGHT - 1)
		    cury = HEIGHT - 1;
                  else
                    HandyMusic_PlaySFX(StepSFX);
		}
	      if (JOY_BTN_RIGHT (joy))
		{
		  curx++;
		  if (curx > WIDTH - 1)
		    curx = WIDTH - 1;
                  else
                    HandyMusic_PlaySFX(StepSFX);
		}
	      if (JOY_BTN_LEFT (joy))
		{
		  curx--;
		  if (curx < 0)
		    curx = 0;
                  else
                    HandyMusic_PlaySFX(StepSFX);
		}
	      if (JOY_BTN_FIRE (joy) && (WaitForRelease == 0))
		{
		  WaitForRelease = 1;
                  HandyMusic_PlaySFX(PickSFX);
		  if (status == GAME_WON)
		    {
		      reset = 1;
		    }
		  if (status == GAME_LOST)
		    {
		      reset = 1;
		    }
		  SelectSquareOrAdjacent (curx, cury);
                  fillcnt = 8;
                  while ((status != GAME_LOST) && (status != GAME_WON) && (fillcnt > 0))
                    {
                      fillcnt -= 1;
		      if (fillpath () == 1)
			{
			  status = GAME_WON;
			}
                    }
		  if (status == GAME_WON && heartstep > 9)
		    {
                      HandyMusic_StopMusic();
		      heartstep = 0;
		      hearttimer = clock () + 30;
		    }
		}
	      if (JOY_BTN_FIRE2 (joy) && (WaitForRelease == 0))
		{
                  HandyMusic_PlaySFX(PickSFX);
		  WaitForRelease = 1;
		  MarkBomb (curx, cury);
		}
	    }
	  if (status == GAME_WON && heartstep < 9 && clock () > hearttimer)
	    {
	      heartstep++;
	      hearttimer = clock () + 30;
              if (heartstep == 2)
                {
                  openn((int)&SAMPLES_FILENR);
                  PlayPCMSample(0);
                }
	    }
	  drawscreen ();
	  tgi_updatedisplay ();
	}
    }
}
