import sys
import json

dir=str(sys.argv[len(sys.argv)-1])
fname= dir + '.sfx'
with open(fname, 'rb') as f:
   raw = f.read()
data = raw

def printsfx(f, data, w, s, nrofsfx):
    f.write('const unsigned char ' + s + '[] = {')
    j = 0;
    i = -1
    for d in data:
        if j < nrofsfx:
            if i > -1: 
                f.write(',')
            else:
                i = 0
                f.write('\n    // HandyMusic_SFX_ATableLo')
            if i == 0:
                f.write('\n    ')
            i = i + 1
            j = j + 1
            f.write('0x' + format(d, '02x'))
        else:
            if j == nrofsfx:
                i = -1
            if j < 2*nrofsfx:
                if i > -1: 
                    f.write(',')
                else:
                    i = 0
                    f.write(',\n    // HandyMusic_SFX_ATableHi')
                if i == 0:
                    f.write('\n    ')
                i = i + 1
                j = j + 1
                f.write('0x' + format(d, '02x'))
            else:
                if j == 2*nrofsfx:
                    i = -1
                if j < 3*nrofsfx:
                    if i > -1: 
                        f.write(',')
                    else:
                        i = 0
                        f.write(',\n    // HandyMusic_SFX_PTable')
                    if i == 0:
                        f.write('\n    ')
                    i = i + 1
                    j = j + 1
                    f.write('0x' + format(d, '02x'))
                else:
                    if j == 3*nrofsfx:
                        i = -1
                    if i > -1: 
                        f.write(',')
                    else:
                        i = 0
                        f.write(',\n    // HandyMusic_SFX_Effects')
                    if i == 0:
                        f.write('\n    ')
                    i = i + 1
                    j = j + 1
                    if i >= w:
                        i = 0
                    f.write('0x' + format(d, '02x'))
    f.write('\n};\n')
    f.write('\n')

fname= dir + '.c'
with open(fname, 'w') as f:
    printsfx(f, data, 8, dir, 7)
