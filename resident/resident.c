/*
 * The code and data defined here is placed in resident RAM
 */
#include <lynx.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <time.h>
#include "LynxSD.h"

#define EEPROM_MAGIC "GAME"	// Change this to tour own string. 3-5 chars are fine.

signed char SDCheck = -1;	// tracks if there is a SD cart with saves enabled. -1 = No check yet, 0 = no SD saves, 1 = SD saves possible 
const char SDSavePath[] = "/saves/game.sav";	//rename game.sav to yourprogramname.sav, i.e. the same name of your lnx file
unsigned int saveBuf[64];	// Buffer for storing SaveData in memory. Only the first 6 bytes are used for checking if data is valid, for the others there is no predefined data structure. Use it as you want

unsigned char halted = 0;	// for pause event
unsigned char reset = 0;	// for reset event
unsigned char bgmusic;		// use bg music
unsigned char mode = 0;
#define GAME_READY 9
#define GAME_WAIT 13
int status = GAME_WAIT;

// For reading writing EEPROM. The data is always 2 bytes, the address range 0..63
extern int __fastcall__ lnx_eeprom_read (unsigned char pos);
extern void __fastcall__ lnx_eeprom_write (unsigned char pos, int val);

extern void HandyMusic_Init ();
extern void HandyMusic_PlayMusic ();
extern void HandyMusic_Pause ();
extern void HandyMusic_UnPause ();
extern void HandyMusic_Main ();
extern void HandyMusic_StopAll ();
extern void HandyMusic_StopMusic ();
extern void __fastcall__ HandyMusic_LoadPlayBGM (unsigned char filenr);
extern unsigned char Sample_Playing;
#pragma zpsym("Sample_Playing");
#define PickSFX 0
#define PlingSFX 1
#define BombSFX 2
#define MachinegunSFX 3
#define ChopperSFX 4
#define ChopperSFXpriority 236
#define AlertSFX 5
#define ExplosionSFX 6
#define StepSFX 7
#define LocknloadSFX 8
extern void __fastcall__ HandyMusic_PlaySFX(unsigned char sfx);

extern void initsystem ();
extern int intro ();
extern int game ();

extern int INTRO_FILENR;
extern int GAME_FILENR;
extern int HM_FILENR;
extern int SFX_FILENR;
extern int MUSIC_FILENR;

unsigned char backupPal[32];	// stores the palette before graying the screen in pause mode
unsigned char grayPal[32];	// stores the grayed palette for the pause mode derived from the active palette

void
eewrite (unsigned char pos, unsigned int val)
{
  unsigned int check;
  check = lnx_eeprom_read (pos);
  if (check != val)
    lnx_eeprom_write (pos, val);
}

void
writeSaveData (void)
{
  unsigned char i;

  if (SDCheck == 1)		// use SD
    {
      if (LynxSD_OpenFile (SDSavePath) == FR_OK)
	{
	  LynxSD_WriteFile ((void *) saveBuf, 128);
	  LynxSD_CloseFile ();
	}
    }
  else if (SDCheck == 0)	// Try to use EEPROM
    {
      for (i = 0; i < 64; i++)
	{
	  eewrite (i, saveBuf[i]);
	}
    }
}

void
readSaveData (void)
{
  unsigned char i;
  FRESULT res;

  if (SDCheck == -1)		// SD not tested yet
    {
      res = LynxSD_OpenFileTimeout (SDSavePath);
      if (res == FR_OK)
	SDCheck = 2;		// SD savefile is ok and next SD access doesn't need to open the file (already opened)
      else if (res == FR_DISK_ERR)	// SD read Timeout -> NO SD
	{
	  SDCheck = 0;		// Try to use EEPROM, if there isn't an EEPROM the functions calls are safe because they don't freeze the code 
	}
      else
	{
	  SDCheck = -2;		// The SD answers, but there is no sav file, or some other problem occoured accessing the file -> don't try to use SD and EEPROM anymore (EEPROM calls on SD frezes the code)
	}
    }

  if (SDCheck == 0)		// Read the EEPROM      
    {
      for (i = 0; i < 64; i++)
	{
	  saveBuf[i] = lnx_eeprom_read (i);
	}
    }
  else if (SDCheck > 0)		// the value can be 1 or 2
    {
      if (SDCheck == 1)		//SD sav file enabled
	{
	  if (LynxSD_OpenFile (SDSavePath) == FR_OK)
	    {
	      LynxSD_ReadFile ((void *) saveBuf, 128);
	      LynxSD_CloseFile ();
	    }
	}
      else			// //SD sav file enabled and sav file already opened
	{
	  SDCheck = 1;
	  LynxSD_ReadFile ((void *) saveBuf, 128);
	  LynxSD_CloseFile ();
	}
    }
}

void
resetSaveData (void)
{
  int i;
  saveBuf[3] = 0;
  strcpy ((char *) saveBuf, EEPROM_MAGIC);
  for (i = 14; i <= 64; i++)
    {
      saveBuf[i] = 0;		//instead of this you could initialize the savedata with some starting values, like a predefined highscores table.
    }
  writeSaveData ();
}



int
main (void)
{
  lynx_load ((int) &INTRO_FILENR);
  initsystem ();
#if 0
  readSaveData ();
  if (strcmp ((char *) saveBuf, EEPROM_MAGIC) != 0)
    resetSaveData ();
#endif


  HandyMusic_StopAll ();
  bgmusic = 1;
  while (1)
    {
      reset = 0;
      halted = 0;

      lynx_load ((int) &INTRO_FILENR);
      intro ();
      lynx_load ((int) &GAME_FILENR);
      game ();
    }
  return 0;
}

static unsigned char WaitForRelease = 0;
static unsigned char WaitForKeyRelease = 0;

unsigned char
checkInput (void)
{
  unsigned int col;
  unsigned char i;
  const unsigned char *pal;

  if (!reset)
    do
      {
	    if (kbhit ())
	      {
                if (WaitForKeyRelease == 0)
                  {
                    WaitForKeyRelease = 1;
	            switch (cgetc ())
	              {
	              case 'F':
		        tgi_flip ();
		        break;
	              case 'P':
		        if (halted)
		          {
		            halted = 0;
		            tgi_setpalette (backupPal);	// restore normal palette. No need to wait vsync; data in the screenbuffer are valid
		            HandyMusic_UnPause ();
		          }
		        else
		          {
		            pal = tgi_getpalette ();	// let's backup the palette
		            for (i = 0; i < 16; i++)
// A simple grayed palette is obtained setting the new r,g, b to the g*0,5 + r*0,25 + b*0,25 . 
// This is an approximate formula that gives a good result considering that there are only 16 shades of gray.                                   
		              {
			        backupPal[i] = pal[i];
			        backupPal[i + 16] = pal[i + 16];
			        col =
			          ((backupPal[i] & 0xf) * 2 +
			           (backupPal[i + 16] & 0xf) +
			           (backupPal[i + 16] >> 4)) >> 2;
			        grayPal[i] = col;
			        grayPal[i + 16] = col | (col << 4);
		              }
		            tgi_setpalette (grayPal);	// set gray palette

		            halted = 1;
		            HandyMusic_Pause ();
		          }
		        break;
	              case 'R':
		        if (halted)
		          {
		            halted = 0;
		            tgi_setpalette (backupPal);	//restore normal palette
		            HandyMusic_UnPause ();
		          }
		        HandyMusic_StopAll ();
		        HandyMusic_LoadPlayBGM (0);
		        reset = 1;
		        break;

	              case '1':
		        if (status == GAME_READY)
		          {
		            mode += 1;
		            if (mode > 2)
		              mode = 0;
                            HandyMusic_PlaySFX(LocknloadSFX);
		          }
		        break;
	              case '2':
		        bgmusic = 1 - bgmusic;
		        if (bgmusic)
		          {
		            HandyMusic_LoadPlayBGM (0);
		          }
		        else
		          {
		            HandyMusic_StopMusic ();
		          }
		        break;

	              case '3':	// used to clear saves on eeprom pressing Opt1 + Opt2 while game is paused
		        if (halted)
		          {
		            resetSaveData ();
		          }
		        break;

	              case '?':
		        break;

	              default:
		        break;
	              }
	          }
	      }
            else
              {
                WaitForKeyRelease = 0;
              }
      }
    while (halted && !reset);

  return joy_read (JOY_1);
}
