#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <conio.h>
#include <6502.h>
#include <time.h>

unsigned char checkInput (void);
extern unsigned char reset;
extern void HandyMusic_Init ();
extern void HandyMusic_PlayMusic ();
extern void HandyMusic_Pause ();
extern void HandyMusic_UnPause ();
extern void HandyMusic_Main ();
extern void HandyMusic_StopAll ();
extern void HandyMusic_StopMusic ();
extern void __fastcall__ HandyMusic_LoadPlayBGM (unsigned char filenr);
extern void __fastcall__ PlayPCMSample(unsigned char sample);
extern void __fastcall__ openn(int filenr);
extern unsigned char bgmusic;
#define GAME_WON 10
#define GAME_LOST 11
extern int status;

extern int HM_FILENR;
extern int SFX_FILENR;
extern int MUSIC_FILENR;
extern int TILE_FILENR;
extern int SAMPLES_FILENR;
extern int SAMPLES1_FILENR;
extern unsigned char Sample_Playing;
#pragma zpsym("Sample_Playing");

extern unsigned char bg[];

static SCB_REHV_PAL Sbg = {
  0xc0, 0x10, 0x01,
  0,
  bg,
  0, 0,
  0x100, 0x100,
  {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF}
};

static char black_pal[] = {
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,
  0x0000 >> 8,

  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
  0x0000 & 0xff,
};

static unsigned char lcdpalette[] = {
  0x222 >> 8,
  0x625 >> 8,
  0x588 >> 8,
  0x558 >> 8,
  0x666 >> 8,
  0x6c5 >> 8,
  0x696 >> 8,
  0x6ad >> 8,
  0x9c6 >> 8,
  0x998 >> 8,
  0x9c9 >> 8,
  0x99c >> 8,
  0xadd >> 8,
  0xc9e >> 8,
  0xccf >> 8,
  0xddd >> 8,

  0x222 & 0xff,
  0x625 & 0xff,
  0x588 & 0xff,
  0x558 & 0xff,
  0x666 & 0xff,
  0x6c5 & 0xff,
  0x696 & 0xff,
  0x6ad & 0xff,
  0x9c6 & 0xff,
  0x998 & 0xff,
  0x9c9 & 0xff,
  0x99c & 0xff,
  0xadd & 0xff,
  0xc9e & 0xff,
  0xccf & 0xff,
  0xddd & 0xff
};

void
initsystem ()
{
  lynx_load ((int) &SFX_FILENR);
  lynx_load ((int) &HM_FILENR);
  tgi_install (&tgi_static_stddrv);	// This will activate the Lynx screen 
  joy_install (&joy_static_stddrv);	// This will activate the Lynx joypad
  tgi_init ();
  tgi_setpalette (black_pal);
  tgi_setframerate (60);
  HandyMusic_Init ();
  CLI ();
  while (tgi_busy ());
  tgi_init ();
  while (tgi_busy ());
}

void
intro (void)
{
  while (tgi_busy ());
  tgi_setpalette (lcdpalette);
  tgi_sprite (&Sbg);
  tgi_updatedisplay ();
  if (status != GAME_LOST)
    {
      clock_t now = clock() + 60;
      openn((int)&SAMPLES1_FILENR);
      PlayPCMSample(1);
      while (clock() < now) ;
      while (Sample_Playing) ;
    }
  if (bgmusic && status != GAME_LOST)
    {
      HandyMusic_LoadPlayBGM (0);
    }
  reset = 0;
  while (checkInput () != 0)
      reset = 0;
  while (checkInput () == 0)
      reset = 0;
}
