all:
	"$(MAKE)" -C game;
	"$(MAKE)" -C intro;
	"$(MAKE)" -C resident;
	"$(MAKE)" -C music;
	"$(MAKE)" -C sfx;
	"$(MAKE)" -C samples;
	"$(MAKE)" -C cart;

clean:
	"$(MAKE)" -C cart clean;
	"$(MAKE)" -C game clean;
	"$(MAKE)" -C intro clean;
	"$(MAKE)" -C resident clean;
	"$(MAKE)" -C music clean;
	"$(MAKE)" -C sfx clean;
	"$(MAKE)" -C samples clean;

