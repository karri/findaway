import os
import numpy as np

print('.export _findawaytomyheart')
print('.segment "SAMPLES1_RODATA"')
print('_findawaytomyheart:')
fname='findawaytomyheart.wav'
with open(fname, 'rb') as f:
    data = f.read()
skipstart = 64
blank = 0
cnt = 0
for b in data:
    cnt = cnt + 1
    if (cnt > skipstart) and (cnt < 21000 + skipstart):
        if (cnt + skipstart < blank + skipstart):
            print('.byte ', 0)
        else:
            b = b - 128
            if b > 0:
                print('.byte ', b)
            else:
                print('.byte ', 255 + b)
